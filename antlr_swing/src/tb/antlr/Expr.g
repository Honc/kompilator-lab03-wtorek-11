grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    
    | PRINT expr NL -> ^(PRINT expr)
    //odczyt blokow instrukcji
    | LB NL -> LB
    | RB NL -> RB
    | if_stat NL -> if_stat
    | NL ->
    ;

if_stat:
    IF^ expr THEN!(stat) (ELSE! (stat));

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
    : atom
      ( POW^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF
  : 'if'
  ;

ELSE
  : 'else'
  ;

THEN
  : 'then'
  ;

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;
	
LB
  : '{'
  ;

RB
  : '}'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^'
  ;